let header = document.querySelector('header');
let startButton = document.querySelector('#startButton');
let bigContain = document.querySelector('#bigcontain');
const son = document.querySelector('#son');
const son2 = document.querySelector('#son2');
const son3 = document.querySelector('#son3');
const img1 = document.querySelector('#img1')
const container = document.querySelector('#questions-content')
const answerQuestion = document.querySelector('#answercontent')
const btnOff = document.querySelector('#use3')




// bouton start pour lancer le quizz ( qui display block mon quizz au click )
startButton.addEventListener('click', () => {
  bigContain.style.display = 'block';
  header.style.display = 'none';
  son.play();


})



// je crée ma classe 'type' question = text / choices = choix / answer = la réponse correcte
class Question {
  constructor(text, choices, answer) {
    this.text = text;
    this.choices = choices
    this.answer = answer;

  }
  /**
   * Méthode qui permet de valider si la reponse du joueur est la bonne
   * @param {string} choice 
   * @returns que le choix est correct
   */
  isGoodAnswer(choice) {
    return this.answer === choice;


  }

}



// je crée mon tableau qui comporte mes instances de Question
let questions = [new Question('Qu est ce qu un cépage?', ['la variété du raisin', 'la suite du bépage', 'un vigneron qui sait'], 'la variété du raisin'),
new Question('Le vin est fait principalement de?', ['ben d alcool', 'd eau', 'sucre'], 'd eau'),
new Question('Un sommelier est ?', ['un vendeur de sommier', 'une cave', 'un professionnel au service du vin'], 'un professionnel au service du vin'),
new Question('Un Magnum contient?', ['6 balles', '150cl', '90cl'], '150cl'),
new Question('Quel est le comble pour un sommelier malade?', ['D être sous Médoc', 'Ne plus avoir de nez', 'Merci Annaëlle pour ta question naze'], 'Merci Annaëlle pour ta question naze'),
new Question('Pour optimiser une dégustation il faut :', ['Une température élevée', 'Secouer fortement la bouteille', 'Ne pas avoir bu la veille', 'Eviter de se parfumer'], 'Eviter de se parfumer'),
new Question('Tout au long d un repas il est conseillé de boire:', ['Du plus jeune au plus vieux (vin)', 'Du plus vieux au plus jeune', 'On s en moque tant qu il y à boire', 'Toujours du blanc au rouge'], 'Du plus jeune au plus vieux (vin)'),
new Question('Les glaçons dans le Champagne </br> c est :', ['Clairement un scandale', 'La mer jaune', 'Une piscine', 'Du gachis, il y a moins de Champ'], 'Une piscine'),
new Question('<img src= "botrytis-cinerea-raisin.jpeg" width="200" height="200" style="border-radius: 50%" > Quel est ce dépot sur le raisin ?', ['la pourriture noble', 'moisissure typée', 'la pourriture d or', 'un raisin pas propre'], 'la pourriture noble'),
new Question('<img src= "cité.jpeg" width="200" height="200" style="border-radius: 50%"> Où se trouve cet étrange bâtiment ?', ['En Nouvelle-Orléans', 'Au Japon', 'A Bordeaux', 'En Normandie'], 'A Bordeaux'),
new Question('<img src= "vendanges.jpeg" width="200" height="200" style="border-radius: 50%"> Quel est ce processus réalisé au cours de l année ?', ['La collecte', 'Les vendanges', 'La beuverie', 'Les récoltes'], 'Les vendanges'),
new Question('<img src= "foudre.jpeg" width="200" height="200" style="border-radius: 50%"> Comment se nomme ce gigantesque contenant?', ['Le tonneau++', 'Le MégaFût', 'Le Foudre', 'La Tonnerre'], 'Le Foudre')]


// je crée ma classe 'type' progress  qui va contenir l'avancée des questions ainsi que l'incrémentation ou non du score
class Progress {
  constructor(questions) {
    this.score = 0;
    this.questions = questions;
    this.indexQuestions = 0;
  }

  /**
   * Méthode qui permet d'indiquer l'index actuel de la question 
   */

  indexQuestionActuelle() {
    return this.questions[this.indexQuestions]
  }

  /**
   * Méthode qui permet d'incrémenter le score si la réponse est juste / dans tous les cas de passer à la question suivante
   * @param {string} answer 
   */
  user(answer) {
    if (this.indexQuestionActuelle().isGoodAnswer(answer)) {
      this.score++;
      son3.play();




    } else {
      son2.play();

    }

    this.indexQuestions++;


  }





  /**
   * Méthode qui permet de signaler la fin du quizz ( fin de l'index questions )
   * @returns l'index de fin
   */
  isEnd() {
    return this.indexQuestions >= this.questions.length;
  }


}



let visiBility = {
  /**
   * fonction qui permet de relier le quiz aux id présents dans le html afin
   *  qu'ils soient visibles
   * @param {id} id 
   * @param {text} text 
   */
  elementShown: function (id, text) {
    let element = document.getElementById(id);
    element.innerHTML = text;
  },

  /**
   * fonction qui permet d'ecrire un texte en rapport avec ton score + l'affichage du score à la fin du quiz
   */
  endQuiz: function () {

    if (quiz.score <= 8) {
      let endQuizHTML = `
  <h3> Bon il va falloir déguster un peu plus : ${quiz.score} / ${quiz.questions.length}</h3> `;

      this.elementShown('questionNum', endQuizHTML); //le texte( by endQuizHtml ) est intégré dans l'id 'questionNum' 
    } else {
      let endQuizHTML2 = ` 
    <h3> Je vois que tu es un fin connaisseur : ${quiz.score} / ${quiz.questions.length}</h3> `;
      this.elementShown('questionNum', endQuizHTML2);

    }

    this.elementShown('questions', 'Le quiz est terminé!!!!'); // le texte 'quizz est terminé' est intégré dans l'id 'questions
    answerQuestion.style.display = 'none';

  },


  /**
   * Fonction qui permet d'afficher une nouvelle question à chaque tour
   */
  question: function () {
    this.elementShown('questions', quiz.indexQuestionActuelle().text) // j'affiche mes questions de mon index actuel dans l'id 'questions'
  },



  /**
   * fonction qui permet permet de relier les choix à la bonne question
   */
  choices: function () {

    let choices = quiz.indexQuestionActuelle().choices;


    /**
     * fonction qui permet d'enregristrer le choix de l'uilisateur au click
     * @param {string} id id de mon bouton
     * @param {string} user reponse sélectionnée
     */
    function userRep(id, user) {
      document.getElementById(id).onclick = function () {

        quiz.user(user);
        quizApp();
      }

    }

    for (let i = 0; i < choices.length; i++) {
      this.elementShown('choice' + i, choices[i])
      userRep('use' + i, choices[i]);
    }

    displayBtn();
  },

  /**
   * fonction qui me permet d'afficher ma progression ( des questions ) en temps réel
   */
  progres: function () {
    let nbrQuestionActuelle = quiz.indexQuestions + 1;
    this.elementShown('questionNum', 'Question ' + nbrQuestionActuelle + ' sur ' + quiz.questions.length);
  }


}

function quizApp() {
  if (quiz.isEnd()) { // lorsque le quizz est terminé, afficher les élements programmés dans "endQuiz"
    visiBility.endQuiz();
  } else {
    visiBility.question(); // afficher les questions
    visiBility.choices(); // afficher les choix 
    visiBility.progres(); // affichage de l'index de questions 
  }
}

/**
 * fonction qui me permet d'ajouter un nouveau bouton 'choice' à partir de ma question 6
 */
function displayBtn() {
  if (quiz.indexQuestions >= 5) {
    btnOff.style.display = 'block'
  } else {
    btnOff.style.display = 'none'
  }

}




let quiz = new Progress(questions);
quizApp();





